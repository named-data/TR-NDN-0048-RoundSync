LATEX = pdflatex
BIBTEX = bibtex
FILE = roundsync
IMAGES = images
GRAFFLE_EXPORT = graffle-export/graffle.sh

.PHONY : clean images all distclean

all : ${FILE}.pdf

${FILE}.pdf : ${FILE}.tex images
	${LATEX} ${FILE}.tex && \
	${BIBTEX} ${FILE} && \
	${LATEX} ${FILE}.tex && \
	${LATEX} ${FILE}.tex


IMAGE_FILES=$(shell ls -d ${IMAGES}/*.graffle)

images : $(IMAGE_FILES:graffle=pdf)

$(IMAGES)/%.pdf : ${IMAGES}/%.graffle
	(cd ${IMAGES} && \
	for f in $?; do \
      base=`basename $$f .graffle`; \
      ${GRAFFLE_EXPORT} $${base}.graffle $${base}.pdf; \
    done\
    )

view: ${FILE}.pdf
	open ${FILE}.pdf

clean :
	find . -name "${FILE}.*" | grep -iv tex | grep -iv bib | xargs rm -f

distclean: clean
	find ${IMAGES} -name "*.pdf" | xargs rm -f
