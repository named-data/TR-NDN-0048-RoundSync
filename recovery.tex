\subsection{Recovery from Cumulative Inconsistency}
\label{sec:recovery}

% Simultaneous data production in the same round, or a lost data packet,
% can cause a node not to receive data packts produced by other nodes.

% The reception of round digests in Sync Interests allows a node to
% detect it is missing data in a certain round, and then fetch it. 

% But if a node $B$ does not receive a Data packet produced by another
% node $A$ in round $N$, and it does not receive the Sync Interest with
% $rd_N$ informing of the production of that data packet, $B$ could not
% detect it is unsynchronized.

% If $A$ keeps producing new data packets, $B$'s application would still
% be able to retrieve the missing user data, thanks to the monotonically
% increasing sequence numbers of user data names used by ChronoSync.

% But if $A$ does not produce new data after the data packet that was
% not received by $B$, $B$ would not have a way to detect it is
% unsynchronized.
%
%If a sync node $N_d$ gets disconnected from the rest of the group for an extended period of time and reconnected to the group again, it may miss the updates from many previous rounds, for which all the other nodes have long stopped exchanging Data Interests and Sync Interests.
%As other nodes keep publishing new data into future rounds, the unsynchronized node $N_d$ may eventually discover the missing data by inspecting the sequence number carried in the application data names.
%However, if any node stops generating new data after $N_d$ rejoins the group, the latter will have no way of learning its missing data published by the former.  To handle such situations, 
%%
%% LZ: I commented out the above -- a node can easily tell if it is falling behind by comparing its own known round# with what it hears, without cumulative digest
%%
In addition to round-by-round synchronization, RoundSync provides a simple way to detect the overall dataset inconsistency using \emph{cumulative digests}, 
%% which enables a node to quickly detect all missing updates in earlier rounds.
and designs a recovery mechanism to reconcile the inconsistency discovered by the cumulative digests.

\subsubsection{Cumulative digests}

% RoundSync uses cumulative digests to detect missing data that the
% round digest mechanism did not detect due to the loss of a Sync
% Interest. The cumulative digest of a round is calculated over the
% whole Data Names Collection known in that round, with the same
% algorithm used to calculate the round digest.

% Cumulative digests are sent piggybacked with the content of ChronoSync
% data packets. When a node produces a data packet in round $N$ ($N$ is
% the last component of its name), it piggybacks 3 additional fields
% related to the cumulative digest of a round $M$:
% \begin{itemize}
% \item unicast prefix of the sender of the data
% \item number of round $M, M < N$
% \item cumulative digest for round $M$ 
% \end{itemize}

% In case a node stops producing new data, it sends an empty data packet
% that only includes the cumulative digest to keep other nodes updated
% about its cumulative digest.

Different from a round digest which covers only the data published in a specific round, the cumulative digest for round $n$ covers the whole dataset observed by each sync node in that round.
Whenever a node produces new data, it piggybacks the cumulative digest for the latest stable round $n$ in the reply packet to the Data Interest in the current round $m$.
The piggybacked information in the data reply includes the following three pieces:
\begin{itemize}
\item the node prefix of the producer who generates this data reply, 
\item the value of the cumulative digest for round $n$, and 
\item the round number ($n$) that the cumulative digest is generated for; note that $n < m$, as the current round $m$ is yet to stabilize.
\end{itemize}

Upon receiving a Data Interest reply with the cumulative digest for round $n$,
\begin{itemize}
\item if the node has not calculated a cumulative digest for that round yet (e.g., because it is still actively synchronizing in the rounds up to $n$), it may delay the processing of the received cumulative digest until it has calculated one itself.
\item if the node has calculated its own cumulative digest($n$), it verifies the consistency of the entire application dataset up to the round $n$.
If the values differ, the node starts the recovery process as described next.
%
% to retrieve the entire application dataset from the node that generated the cumulative digest (which is identified by the unicast node prefix in the piggybacked information).  
\end{itemize}

% \noindent Even if the application stops publishing new data, the sync node should still keep producing empty reply packets that include the cumulative digests of earlier rounds (which is done automatically by the RoundSync service module) in order to inform other nodes about its sync state.

% When a node receives a cumulative digest of round $M$ that has been
% calculated by another node, it can compare it with the local one to
% detect if it is missing data in rounds $<= M$. 

% If the cumulative digests are different, the uncoupled design of
% RoundSync would allow nodes to go round by round, comparing
% previous round digests to discover the unsynchronized rounds, and then
% send Data interest in those rounds. But this peeling process would
% take a long time, for example after a long partition. So, instead of
% it, RoundSync uses a Recovery Mechanism: when cumulative digests
% are different, the receiver requests the whole Data Names Collection
% from the sender by sending a Recovery Interest. This recovery
% mechanism syncs the name dataset without syncing the rounds log.

% Sending cumulative digests of unstable rounds would trigger
% unnecessary recoveries. So, cumulative digests are calculated only for
% rounds that are stable. A round $M$ is stable if neither data packets
% nor Sync Interests have been received for rounds $<=M$ for a while.

% When a node sends a data packet in round $N$,
% it piggybacks on it the cumulative digest of the greatest
% \emph{stable} round $M, M < N$. 

% Figure~\ref{fig:cumulativeDigests} shows the Rounds Log of a node
% that has received Data packets in rounds 1 to 13. The latest rounds it
% has stabilized are rounds 5, 6 and 7. The current greatest stable
% round is 7, since it has been a while since the node received Data in
% rounds $<=7$. So, the node has already calculated cumulative digests
% for rounds $<=7$, and will send the cumulative digest of round $7$
% piggybacked in data packet that it produces.

Note that sending cumulative digests for recent rounds that are yet to achieve complete synchronization may unnecessarily trigger multiple nodes to take recovery actions.
To reduce the chance of premature recovery actions, a sync node should only include the cumulative digest for the latest round $n$ up to which no change has been made for a sufficicently long time.
That is, all the rounds up to $n$ have already stabilized.
For example, in Fig.~\ref{fig:cumulativeDigests} the Rounds Log
includes rounds 1 through 10, and the previous highest stable round number
is 4 whose cumulative digest has been piggybacked in the Data Interest
replies generated for rounds 7 through 10.
After the node detects
that no more data has been produced for rounds 5 through 7 for a long enough
time, it will mark those rounds as stable, calculate the cumulative
digest up to round 7, and piggyback that information in
future Data Interest replies (for the rounds after 10).

\begin{figure}[!t]
\centering
\includegraphics[width=\columnwidth]{figs/figs-tr-05.pdf}
\caption{Stable round and calculation of cumulative digests.}
\label{fig:cumulativeDigests}
\end{figure}


\subsubsection{Recovery process}

% When a node $B$ receives a data packet produced by $A$ in round $N$
% that includes a cumulative digest for stable round $M$, $B$ compares
% it with the cumulative digest it has recorded for round $M$. If they
% are equal, $B$ can assume it is synchronized up to round $M$ with
% producer $A$. If $M$, the stable round of the sender node $A$, is
% greater than B's stable round, B has not yet calculated a cumulative
% digest for round $M$, and so B postpones the comparison.

% If cumulative digests for round $M$ are different, receiver $B$ knows
% that it is unsynchronized with producer $A$ in some unknown round
% $<=M$. But $A$ does no know it, so in order to inform the rest of
% nodes, $B$ sends a new data packet in the current round, that only
% contains its cumulative digest for round $M$\footnote{Similar to a
%   ACK-only TCP segment.}. Because other receivers could have the same
% cumulative digest for round $M$ than $B$, $B$ uses a random timer to
% avoid the situation where multiple nodes that are synched among them,
% end up sending the same cumulative digest\footnote{This mechanism is
%   similar to the one used in Scalable Reliable Multicast.}.

% To sync up, a receiver of a data packet with an unrecognized
% cumulative digest sends a Recovery Interest to the unicast prefix of
% the sender of the data packet. Figure~\ref{fig:namingRules} shows an
% example of a Recovery Interest name. Component (1) is the unicast
% prefix, component (2) allows to demultiplex to the application, and
% component (3) to differentiate among different kinds of interests.

To recover from a cumulative digest inconsistency with node $R$ and $R$'s cumulative digest $D_{R(n)}$, a node $A$ first determines whether
the value of $n$ is very close to $A$'s current round $m$; if so, it means that $A$ is following the group's data production closely and the inconsistency may be recovered by using round-by-round synchronization for a few round right before round $n$. 

If $m \ll n$, or if $A$ fails to re-synchronize after trying a few round-by-round synchronization, $A$ will invoke a recovery process by sending a Recovery Interest using $R$'s unicast prefix learned from the data reply, concatenated with the cumulative digest value $D_{R(n)}$.
%%%
In addition, node $A$ also produces a Data Interest reply to the current round with no update but its own prefix and its cumulative digest for round $n$.  This reply is sent after a small random-wait time; the node suppresses its own reply if another node sends out a similar reply before it does.
Doing so is to inform the remote node $R$ about the inconsistency, triggering $R$ to send a Recovery Interest to $A$ as well.
%% A may not have cumulative digest for round n but for round n1, with n>n1
%%% Wentao's reply: The previous paragraph already handles this case: "(node A) postpones the processing of that cumulative digest until it has calculated the cumulative digest for round n."
%% why need this?  its use is not explained later
%%% Wentao's reply: the use of this empty update is "to make sure that the remote node who generated that cumulative digest also detects the inconsistency"

% A Recovery Interest is replied with a Recovery Data packet that
% contains the current Data Names Collection of the sender, and its current
% round number. 

% The receiver of the Recovery Data updates its Data Names Collection
% with the contents of the received Data Names Collection. A copy of the
% updated Data Names Collection is stored in the \emph{Recovery Data
%   Names Collection}.

% The node receiving a Recovery Data calculates the \emph{Recovery
%   Round} as the maximum between the round associated with the
% received Data Names Collection and its own current round. The Recovery
% Round is considered by the receiving node as its new current round.

% After a recovery, a node must invalidate the cumulative digests of its
% stable rounds, because they were calculated over data names
% collections that did not include the state reflected by the received
% Recovery Data.

% Thus, a recovered node does not have stable rounds, being in a
% situation that is similar to the one of a booting node that starts
% with an empty state. During this period, the recovered node will not
% process the received cumulative digests from other nodes, and will not
% send cumulative digests. If it receives new Data in rounds lower than
% the Recovery Round, the new data names will be added to the Recovery
% Data Names Collection.

% After a while, once the Recovery Round is considered stable, the
% Recovery Data Names Collection is used to calculate the cumulative
% digest of the Recovery Round, and the process of calculating new
% stable rounds can proceed as usual.

% Because cumulative digests in rounds previous to the Recovery Round
% are invalid, the Rounds Log can be truncated.

Note that, except special cases where every node publishes at each round, in general nodes do not keep a complete list of every node's sequence number, i.e. the entire application dataset state, for every round.
Therefore when a node receives a Recovery Interest for round $n$, it cannot reply with the application dataset state for that round. 
Instead, the reply to a Recovery Interest contains the current application
dataset state and the current round number $m$ of the sender. 
 
Node $A$ who receives the recovery reply data packet updates its own application dataset with the received one and stores the updated dataset temporarily in a data structure called \emph{recovery data names collection}.  Node $A$ also moves to round $m$ if $m$ is greater than $A$'s current round number.
The new round is designated as the \emph{recovery round}.
Since its previous sync state is unsynchronized, the node will delete all the
cumulative digests it has calculated before, and purge the Rounds Log to reclaim storage.  
Once the recovery round becomes stable (i.e., no data is published in that round for sufficiently long time),
the node calculates the cumulative digest for the recovery round using
the dataset stored in the recovery data names collection, and starts
calculating cumulative digests for the future rounds.

% Figure~\ref{fig:recoveryRound} shows a node that received a Recovery
% Data while it was in current round number 71. The received Recovery
% Data included the Data Names Collection of a node that was in current
% round 103. Thus, the Recovery Round was set to $103 = max(71, 103)$,
% and the union of the received Data Names Collection and its own was
% recorded in the Recovery Data Names Collection shown in the
% figure. 

% Once the Recovery Round stabilized, entries of the Rounds Log below
% the Recovery Round were truncated (shaded in the figure). The Recovery
% Data Names Collection was used to calculate cumulative digests of
% rounds 104, 105 and 106. 

Fig.~\ref{fig:recoveryRound} shows an example of the recovery process.
When a recovery reply containing the application dataset and round
number 103 is received, the node who is currently in round 71 updates
its own application dataset and jumps to round 103 immediately,
marking it as the recovery round.  It also truncates its original
Rounds Log and removes all entries before round 71.  After round 103
through 106 is stabilized, the node uses the recovery data names
collection and the additional updates received in each round to
calculate the cumulative digests for rounds 103 to 106.
%
%We note that the round number serves as a logical clock, that enables each node to gauge how far it may be falling behind others in order to take proper recovery steps.
%
%% there are a number of questions regarding the recovery process
%% 1/ the question earlier about sending an empty data reply
%%% Already replied earlier.
%% 2/ A can have its own cumulative digest(n1), receive cumulative digest(n2):
%%    should A include n1 in its recovery interest?
%% 3/ given we have round digest, why not just start recovery by first send
%%    a vector of round digests?  I guess we dont save round digests after a round is stabilized?  when to delete a round digest is not discussed
%%% Wentao's reply: I think 2/ and 3/ are design alternatives that definitely will work. Maybe we can explore those approaches in future work (if someone is interested in taking over the RoundSync work)?


\begin{figure}[!t]
\centering
\includegraphics[width=\columnwidth]{figs/figs-tr-06.pdf}
\caption{Updating Rounds Log after receiving recovery reply}
\label{fig:recoveryRound}
\end{figure}

